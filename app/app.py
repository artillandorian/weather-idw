from fastapi import FastAPI, Request, Form, Response
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from datetime import timedelta, date, datetime

import requests


from models.activity_model import ActivityModel

from starlette.responses import Response

#   Functions
from models.functions import selectLanguage
from models.functions import userLogin
from models.UserModel import UserModel
from models.ProjectModel import ProjectModel
from models.GroupModel import GroupModel


import uvicorn
import requests as re
import json


import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("serviceAccountKey.json")
firebase_admin.initialize_app(cred)

db=firestore.client()
app = FastAPI()


app.mount("/assets", StaticFiles(directory="assets"), name="assets")

#   Creation of template management by Jinja
templates = Jinja2Templates(directory="templates")



class Settings():
    connected: bool = False
    admin: bool = False
    name: str = ""
    idUser: str = ""


settings = Settings()


#   Home Page GET
@app.get("/", response_class=HTMLResponse)
async def home(request: Request):
    if(settings.connected == True):
        #   Connected as Admin
        if(settings.admin == True):
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeAdmin", "settings" : settings, "listProject": getProjects()})
        #   Connected as User
        else :
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings})


@app.post("/getWeather", response_class=HTMLResponse)
async def access(request: Request):
    isRegister = False

    shortDateCurrent = datetime.today()
    shortDate = shortDateCurrent + timedelta(days=1)
    shortDateCurrent = shortDateCurrent.strftime('%Y-%m-%d')
    shortDate = shortDate.strftime('%Y-%m-%d')

    ## every cities
    city_name = ""
    city_lat = ""
    city_lon = ""

    send_data = True

    docs_infos = db.collection(u'informations_weathers').stream()
    for doc_info in docs_infos:
        dictionnary = str(doc_info.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        if(shortDate == dictionnaryjson['date'] and dictionnaryjson['city_id'] == '0qflo5v1nZkys1VLeSQZ'):
            send_data = False
        elif(shortDate == dictionnaryjson['date'] and dictionnaryjson['city_id'] == 'QdjiWF7TlBjDMLr1orrG') :
            send_data = False
        elif(shortDate == dictionnaryjson['date'] and dictionnaryjson['city_id'] == 'w5ylJR4rgQ0v76Jq9LsM') :
            send_data = False

    if(send_data == True):
        docs = db.collection(u'city').stream()
        for doc in docs:
            dictionnary = str(doc.to_dict())
            dictionnary = dictionnary.replace("True", "true")
            dictionnary = dictionnary.replace("False", "false")
            dictionnary = dictionnary.replace("'", '"')
            dictionnaryjson = json.loads(dictionnary)
            city_name = dictionnaryjson['name']
            city_accu_weather_id = dictionnaryjson['accu_weather_id']
            city_lat = dictionnaryjson['lat']
            city_lon = dictionnaryjson['lon']
            city_id = doc.id
            print(city_id)

            ## get result from prevision-meteo but current time
            api_result = requests.get('https://www.prevision-meteo.ch/services/json/lat='+city_lat+'lng='+city_lon)
            api_response = api_result.json()
            api_response_openweathermap = api_response

            midi = api_response['fcst_day_0']['hourly_data']['12H00']
            midi_condition = midi['CONDITION_KEY']
            prevision_meteo_sky = "Clouds"
            if(midi_condition == "eclaircies" or midi_condition == "ensoleille"):
                prevision_meteo_sky = "Clear"
            

            float_kelvin = float(midi['TMP2m'])
            float_kelvin = float_kelvin + 273.15


            data = {
                u'date': u''+shortDateCurrent+'',
                u'temp_kelvin': u''+str(float_kelvin)+'',
                u'api_name': u'exact',
                u'city_id': u''+city_id+'',
                u'weather': u''+prevision_meteo_sky+''
            }

            db.collection(u'informations_weathers').document(u''+shortDateCurrent+'-'+'exact'+'-'+city_id).set(data)

            #db.collection('informations_weathers').add({'date': shortDateCurrent, 'temp_kelvin' : float_kelvin, 'api_name' : 'exact', 'city_id' : city_id, 'weather' : prevision_meteo_sky})

            ## get result from prevision-meteo but current time

            ## get result from prevision-meteo
            api_result = requests.get('https://www.prevision-meteo.ch/services/json/lat='+city_lat+'lng='+city_lon)
            api_response = api_result.json()
            api_response_openweathermap = api_response

            midi = api_response['fcst_day_1']['hourly_data']['12H00']
            midi_condition = midi['CONDITION_KEY']
            prevision_meteo_sky = "Clouds"
            if(midi_condition == "eclaircies" or midi_condition == "ensoleille"):
                prevision_meteo_sky = "Clear"
            

            float_kelvin = float(midi['TMP2m'])
            float_kelvin = float_kelvin + 273.15


            data = {
                u'date': u''+shortDate+'',
                u'temp_kelvin': u''+str(float_kelvin)+'',
                u'api_name': u'prevision-meteo',
                u'city_id': u''+city_id+'',
                u'weather': u''+prevision_meteo_sky+''
            }

            db.collection(u'informations_weathers').document(u''+shortDate+'-'+'prevision-meteo'+'-'+city_id).set(data)

            #db.collection('informations_weathers').add({'date': shortDate, 'temp_kelvin' : float_kelvin, 'api_name' : 'prevision-meteo', 'city_id' : city_id, 'weather' : prevision_meteo_sky})

            ## get result from prevision-meteo


            ## get result from openweathermap
            api_result = requests.get('https://api.openweathermap.org/data/2.5/onecall?lat='+city_lat+'&lon='+city_lon+'&exclude=daily&appid=81848f40576fc7e26f6622d0e51d9a84')
            api_response = api_result.json()
            api_response_openweathermap = api_response
            prevision_meteo_sky = "Clear"
            if(api_response['hourly'][24]['weather'][0]['main'] != "Clear"):
                prevision_meteo_sky = "Clouds"

            data = {
                u'date': u''+shortDate+'',
                u'temp_kelvin': u''+str(float_kelvin)+'',
                u'api_name': u'openweathermap',
                u'city_id': u''+city_id+'',
                u'weather': u''+prevision_meteo_sky+''
            }

            db.collection(u'informations_weathers').document(u''+shortDate+'-'+'openweathermap'+'-'+city_id).set(data)

            #db.collection('informations_weathers').add({'date': shortDate, 'temp_kelvin' : api_response['hourly'][24]['temp'], 'api_name' : 'open-weather-map', 'city_id' : city_id, 'weather' : prevision_meteo_sky})
            ## get result from openweathermap




            ## get result from weatherbit
            api_result = requests.get('https://api.weatherbit.io/v2.0/forecast/hourly?lat='+city_lat+'&lon='+city_lon+'&key=1e631ec34f5f44de99d31294625e6950&hours=25')

            api_response = api_result.json()

            api_response_openweathermap = api_response
            prevision_meteo_sky = "Clouds"
            if(api_response['data'][24]['weather']['description'] == "Clear Sky"):
                prevision_meteo_sky = "Clear"

            float_kelvin = float(api_response['data'][24]['temp'])
            float_kelvin = float_kelvin + 273.15


            data = {
                u'date': u''+shortDate+'',
                u'temp_kelvin': u''+str(float_kelvin)+'',
                u'api_name': u'weatherbit',
                u'city_id': u''+city_id+'',
                u'weather': u''+prevision_meteo_sky+''
            }

            db.collection(u'informations_weathers').document(u''+shortDate+'-'+'weatherbit'+'-'+city_id).set(data)

            #db.collection('informations_weathers').add({'date': shortDate, 'temp_kelvin' : float_kelvin, 'api_name' : 'weatherbit', 'city_id' : city_id, 'weather' : prevision_meteo_sky})
            ## get result from weatherbit
            
            


    


    
    

    
    
    
    
    return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings})


@app.post("/login", response_class=HTMLResponse)
async def access(request: Request, pseudo: str = Form("pseudo")):
    isRegister = False
    docs = db.collection(u'persons').stream()


    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']
        if(str(dictName).lower() == pseudo.lower()):
            isRegister = True


    if(isRegister == False):
        db.collection('persons').add({'name': pseudo, 'isAdmin': False})


    docs = db.collection(u'persons').stream()

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']
        if(str(dictName).lower() == pseudo.lower()):
            settings.idUser = doc.id
            settings.admin = dictAdmin
            settings.name = dictName
            settings.connected = True
    
    
    if(settings.admin == True and settings.connected == True ):
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeAdmin", "listProject": getProjects(), "settings" : settings})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})



@app.post("/addProject", response_class=HTMLResponse)
async def access(request: Request, title: str = Form("title"), description: str = Form("description"), members: str = Form("members"), groups: str = Form("groups")):
    
    if(settings.connected == True):
        #   Connected as Admin
        if(settings.admin == True):
            nbMembers = int(members)
            nbGroups = int(groups)
            if(nbMembers >= nbGroups):
                # For bloc creation of project with group < 2
                memberPerGroups = nbMembers // nbGroups
                memberRestPerGroups = nbMembers % nbGroups

                if(memberPerGroups >= 2):
                    doc = db.collection('projects').add({'title': title, 'description': description, 'nbMembers': nbMembers, 'nbGroups': nbGroups, 'createBy': settings.idUser})
                    print(doc[1].id)

                    nbGroupsWhile = nbGroups
                    while nbGroupsWhile > 0:
                        newTitle = title + ' - Groupe : ' + str(nbGroupsWhile)
                        if(memberRestPerGroups > 0):
                            db.collection('groups').add({'title': newTitle, 'maxMember': memberPerGroups + 1, 'position': nbGroupsWhile, 'idProject': doc[1].id})
                            memberRestPerGroups = memberRestPerGroups - 1
                        else:
                            db.collection('groups').add({'title': newTitle, 'maxMember': memberPerGroups, 'position': nbGroupsWhile, 'idProject': doc[1].id})

                            
                        nbGroupsWhile = nbGroupsWhile - 1

            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeAdmin", "user" : settings.name, "listProject": getProjects(), "settings" : settings})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 


@app.post("/addUserToProject", response_class=HTMLResponse)
async def access(request: Request, userId: str = Form("userId"), projectId: str = Form("projectId")):

    if(settings.connected == True):
        #   Connected as Admin
        if(settings.admin == True or userId == settings.idUser):
            project = getProject(projectId)
            maxUser = False
            inProject = False
            alreadyInOneGroup = False

            if(getProject(projectId).intListUserInside < getProject(projectId).nbMembers):
                db.collection('list_users_projects').add({'idProject': projectId, 'idUser': userId})
            
            project = getProject(projectId)
            
            if(project.nbMembers <= project.intListUserInside):
                maxUser = True
            
            for user in project.listUserInside:
                if(user.id == settings.idUser):
                    inProject = True

            for groupInList in project.listGroups:
                for userInList in groupInList.listUserInside:
                    if(userInList.id == settings.idUser):
                        alreadyInOneGroup = True
            
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "project", "project": getProject(projectId), "settings" : settings, "maxUser": maxUser, "inProject" : inProject, "alreadyInOneGroup": alreadyInOneGroup})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 


@app.post("/delUserToProject", response_class=HTMLResponse)
async def access(request: Request, userId: str = Form("userId"), projectId: str = Form("projectId")):    
    if(settings.connected == True):
        #   Connected as Admin
        if(settings.admin == True or settings.idUser == userId):
            docss = db.collection(u'list_users_groups').stream()
            for doc in docss:
                dictionnary = str(doc.to_dict())
                dictionnary = dictionnary.replace("{'", '{"')
                dictionnary = dictionnary.replace("'}", '"}')
                dictionnary = dictionnary.replace("': '", '": "')
                dictionnary = dictionnary.replace("', '", '", "')

                dictionnary = dictionnary.replace("':", '":')
                dictionnary = dictionnary.replace("',", '",')

                dictionnary = dictionnary.replace(", '", ', "')
                dictionnary = dictionnary.replace(": '", ': "')

                dictionnaryjson = json.loads(dictionnary)
                dictIdProject = dictionnaryjson['idProject']
                dictIdUser = dictionnaryjson['idUser']
                if(dictIdProject == projectId and dictIdUser == userId):
                    doc.reference.delete()

            docs = db.collection(u'list_users_projects').stream()
            for doc in docs:
                dictionnary = str(doc.to_dict())
                dictionnary = dictionnary.replace("{'", '{"')
                dictionnary = dictionnary.replace("'}", '"}')
                dictionnary = dictionnary.replace("': '", '": "')
                dictionnary = dictionnary.replace("', '", '", "')

                dictionnary = dictionnary.replace("':", '":')
                dictionnary = dictionnary.replace("',", '",')

                dictionnary = dictionnary.replace(", '", ', "')
                dictionnary = dictionnary.replace(": '", ': "')

                dictionnaryjson = json.loads(dictionnary)
                dictIdProject = dictionnaryjson['idProject']
                dictIdUser = dictionnaryjson['idUser']
                if(dictIdProject == projectId and dictIdUser == userId):
                    doc.reference.delete()

            project = getProject(projectId)
            maxUser = False
            inProject = False
            alreadyInOneGroup = False
            if(project.nbMembers <= project.intListUserInside):
                maxUser = True
            
            for user in project.listUserInside:
                if(user.id == settings.idUser):
                    inProject = True

            for groupInList in project.listGroups:
                for userInList in groupInList.listUserInside:
                    if(userInList.id == settings.idUser):
                        alreadyInOneGroup = True

            return templates.TemplateResponse("index.html", {"request": request, "pageName": "project", "project": getProject(projectId), "settings" : settings, "maxUser": maxUser, "inProject" : inProject, "alreadyInOneGroup": alreadyInOneGroup})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 





@app.get("/getProject/{idProject}")
async def access(idProject, response: Response, request: Request):
    if(settings.connected == True):
        project = getProject(idProject)
        maxUser = False
        inProject = False
        alreadyInOneGroup = False

        if(project.nbMembers <= project.intListUserInside):
            maxUser = True
        
        for user in project.listUserInside:
            if(user.id == settings.idUser):
                inProject = True
        
        for groupInList in project.listGroups:
            for userInList in groupInList.listUserInside:
                if(userInList.id == settings.idUser):
                    alreadyInOneGroup = True




        return templates.TemplateResponse("index.html", {"request": request, "pageName": "project", "project": project, "settings" : settings, "maxUser": maxUser, "inProject" : inProject, "alreadyInOneGroup": alreadyInOneGroup})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 

@app.post("/getGroup/", response_class=HTMLResponse)
async def access(request: Request, response: Response, groupId: str = Form("groupId")):
    if(settings.connected == True):
        inProject = False
        inGroup = False
        maxUser = False
        alreadyInOneGroup = False

        group = getGroup(groupId)
        project = getProject(group.idProject)

        for userInList in project.listUserInside:
            if(userInList.id == settings.idUser):
                inProject = True

        for userInList in group.listUserInside:
            if(userInList.id == settings.idUser):
                inGroup = True

        if(group.intListUserInside >= group.maxMember):
            maxUser = True

        for groupInList in project.listGroups:
            for userInList in groupInList.listUserInside:
                if(userInList.id == settings.idUser):
                    alreadyInOneGroup = True
                

        return templates.TemplateResponse("index.html", {"request": request, "pageName": "group", "group": getGroup(groupId), "settings" : settings, "usersInProjectWithoutGroup" : getUsersInProjectNoAffected(groupId), "message": "", "maxUser": maxUser, "inGroup": inGroup, "inProject": inProject, "alreadyInOneGroup": alreadyInOneGroup })
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 


@app.get("/seeUsers", response_class=HTMLResponse)
async def home(request: Request):
    if(settings.admin == True):
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "seeUsers", "settings" : settings, "listUsers": getUsers()})
    else:
        if(settings.connected == True):
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings})

@app.get("/seeMyGroups", response_class=HTMLResponse)
async def home(request: Request):
    if(settings.admin == True):
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeAdmin", "listProject": getProjects(), "settings" : settings})
    else:
        if(settings.connected == True):
            docs = db.collection(u'list_users_groups').stream()
            groups = []

            for doc in docs:
                dictionnary = str(doc.to_dict())
                dictionnary = dictionnary.replace("{'", '{"')
                dictionnary = dictionnary.replace("'}", '"}')
                dictionnary = dictionnary.replace("': '", '": "')
                dictionnary = dictionnary.replace("', '", '", "')

                dictionnary = dictionnary.replace("':", '":')
                dictionnary = dictionnary.replace("',", '",')

                dictionnary = dictionnary.replace(", '", ', "')
                dictionnary = dictionnary.replace(": '", ': "')

                dictionnaryjson = json.loads(dictionnary)
                dictIdGroup = dictionnaryjson['idGroup']
                dictIdUser = dictionnaryjson['idUser']
                if(dictIdUser == settings.idUser):
                    groups.append(getGroup(dictIdGroup))

            return templates.TemplateResponse("index.html", {"request": request, "pageName": "seeMyGroups", "settings" : settings, "groups": groups})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings})


@app.post("/addUserToGroup", response_class=HTMLResponse)
async def access(request: Request, userId: str = Form("userId"), groupId: str = Form("groupId")):

    if(settings.connected == True):
        #   Connected as Admin
        if(settings.admin == True or settings.idUser == userId):
            message = addUserToGroup(userId, groupId)
            inProject = False
            inGroup = False
            maxUser = False
            alreadyInOneGroup = False

            group = getGroup(groupId)
            project = getProject(group.idProject)

            for userInList in project.listUserInside:
                if(userInList.id == settings.idUser):
                    inProject = True

            for userInList in group.listUserInside:
                if(userInList.id == settings.idUser):
                    inGroup = True

            if(group.intListUserInside >= group.maxMember):
                maxUser = True


            for groupInList in project.listGroups:
                for userInList in groupInList.listUserInside:
                    if(userInList.id == settings.idUser):
                        alreadyInOneGroup = True
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "group", "group": getGroup(groupId), "settings" : settings, "usersInProjectWithoutGroup" : getUsersInProjectNoAffected(groupId), "message": message, "maxUser": maxUser, "inGroup": inGroup, "inProject": inProject, "alreadyInOneGroup": alreadyInOneGroup})
        else:
            return templates.TemplateResponse("index.html", {"request": request, "pageName": "homeUser", "settings" : settings, "inside": getProjectInsideOrOutside('inside'), "notInside": getProjectInsideOrOutside('outside')})
    else:
        return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings}) 

@app.post("/delUserToGroup", response_class=HTMLResponse)
async def access(request: Request, userId: str = Form("userId"), groupId: str = Form("groupId")):
    message = ""
    if(settings.admin == True or settings.idUser == userId):
        docs = db.collection(u'list_users_groups').stream()
        for doc in docs:
            dictionnary = str(doc.to_dict())
            dictionnary = dictionnary.replace("{'", '{"')
            dictionnary = dictionnary.replace("'}", '"}')
            dictionnary = dictionnary.replace("': '", '": "')
            dictionnary = dictionnary.replace("', '", '", "')

            dictionnary = dictionnary.replace("':", '":')
            dictionnary = dictionnary.replace("',", '",')

            dictionnary = dictionnary.replace(", '", ', "')
            dictionnary = dictionnary.replace(": '", ': "')

            dictionnaryjson = json.loads(dictionnary)
            dictIdGroup = dictionnaryjson['idGroup']
            dictIdUser = dictionnaryjson['idUser']
            if(dictIdGroup == groupId and dictIdUser == userId):
                doc.reference.delete()
                message = "utilisateur supprimé"

            
            inProject = False
            inGroup = False
            maxUser = False
            alreadyInOneGroup = False

            group = getGroup(groupId)
            project = getProject(group.idProject)

            for userInList in project.listUserInside:
                if(userInList.id == settings.idUser):
                    inProject = True

            for userInList in group.listUserInside:
                if(userInList.id == settings.idUser):
                    inGroup = True

            if(group.intListUserInside >= group.maxMember):
                maxUser = True

            for groupInList in project.listGroups:
                for userInList in groupInList.listUserInside:
                    if(userInList.id == settings.idUser):
                        alreadyInOneGroup = True

    return templates.TemplateResponse("index.html", {"request": request, "pageName": "group", "group": getGroup(groupId), "settings" : settings, "usersInProjectWithoutGroup" : getUsersInProjectNoAffected(groupId), "message": message, "maxUser": maxUser, "inGroup": inGroup, "inProject": inProject, "alreadyInOneGroup": alreadyInOneGroup})



@app.get("/link/{groupId}", response_class=HTMLResponse)
async def home(groupId, response: Response, request: Request):
    group = getGroup(groupId)
    project = getProject(group.idProject)

    maxInProject = False
    maxInGroup = False

    if(project.nbMembers <= project.intListUserInside):
        maxInProject = True
    if(group.maxMember <= group.intListUserInside):
        maxInGroup = True
    
    
    return templates.TemplateResponse("index.html", {"request": request, "pageName": "link", "settings" : settings, "project" : project, "group" : group, "maxInGroup" : maxInGroup, "maxInProject" : maxInProject})

@app.post("/addUserToGroupLink", response_class=HTMLResponse)
async def access(request: Request, userName: str = Form("userName"), groupId: str = Form("groupId")):

    isRegister = False
    docs = db.collection(u'persons').stream()

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']
        if(str(dictName).lower() == userName.lower()):
            isRegister = True


    if(isRegister == False):
        db.collection('persons').add({'name': userName, 'isAdmin': False})
    
    

    docs = db.collection(u'persons').stream()

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']
        if(str(dictName).lower() == userName.lower()):
            settings.idUser = doc.id
            settings.admin = dictAdmin
            settings.name = dictName
            settings.connected = True

    message = addUserToGroup(settings.idUser, groupId)
    inProject = False
    inGroup = False
    maxUser = False
    alreadyInOneGroup = False

    group = getGroup(groupId)
    project = getProject(group.idProject)

    for userInList in project.listUserInside:
        if(userInList.id == settings.idUser):
            inProject = True

    for userInList in group.listUserInside:
        if(userInList.id == settings.idUser):
            inGroup = True

    if(group.intListUserInside >= group.maxMember):
        maxUser = True


    for groupInList in project.listGroups:
        for userInList in groupInList.listUserInside:
            if(userInList.id == settings.idUser):
                alreadyInOneGroup = True
    return templates.TemplateResponse("index.html", {"request": request, "pageName": "group", "group": getGroup(groupId), "settings" : settings, "usersInProjectWithoutGroup" : getUsersInProjectNoAffected(groupId), "message": message, "maxUser": maxUser, "inGroup": inGroup, "inProject": inProject, "alreadyInOneGroup": alreadyInOneGroup})
    

@app.post("/randomGroup", response_class=HTMLResponse)
async def access(request: Request, userId: str = Form("userId"), projectId: str = Form("projectId")):
    project = getProject(projectId)
    isSend = False
    message = ""
    doNoting = True
    for groupInList in project.listGroups:
        if(isSend == False):
            if(groupInList.maxMember <= groupInList.intListUserInside):
                doNoting = True
            else:
                message = addUserToGroup(userId, groupInList.id)
                isSend = True

    project = getProject(projectId)
    maxUser = False
    inProject = False
    alreadyInOneGroup = False

    if(project.nbMembers <= project.intListUserInside):
        maxUser = True
    
    for user in project.listUserInside:
        if(user.id == settings.idUser):
            inProject = True
    
    for groupInList in project.listGroups:
        for userInList in groupInList.listUserInside:
            if(userInList.id == settings.idUser):
                alreadyInOneGroup = True
    

    return templates.TemplateResponse("index.html", {"request": request, "pageName": "project", "project": project, "settings" : settings, "maxUser": maxUser, "inProject" : inProject, "alreadyInOneGroup": alreadyInOneGroup, "message": message})

    





@app.get("/disconnected", response_class=HTMLResponse)
async def home(request: Request):
    settings.idUser = ""
    settings.admin = False
    settings.name = ""
    settings.connected = False
    
    
    return templates.TemplateResponse("index.html", {"request": request, "pageName": "login", "settings" : settings})




# function
def getProjects():
    docs = db.collection(u'projects').stream()
    listProjects = []

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictCreateBy = dictionnaryjson['createBy']
        dictDescription = dictionnaryjson['description']
        dictTitle = dictionnaryjson['title']
        dictNbGroups = dictionnaryjson['nbGroups']
        dictNbMembers = dictionnaryjson['nbMembers']

        project = ProjectModel(doc.id, dictCreateBy, dictDescription, dictTitle, dictNbGroups, dictNbMembers, getGroupsFromProject(doc.id), getUsersFromProject('inside', doc.id), getUsersFromProject('notInside', doc.id))
        listProjects.append(project)

    return listProjects

def getProject(idProject):
    docs = db.collection(u'projects').stream()
    project = ""

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictCreateBy = dictionnaryjson['createBy']
        dictDescription = dictionnaryjson['description']
        dictTitle = dictionnaryjson['title']
        dictNbGroups = dictionnaryjson['nbGroups']
        dictNbMembers = dictionnaryjson['nbMembers']
        if(doc.id == idProject):
            project = ProjectModel(doc.id, dictCreateBy, dictDescription, dictTitle, dictNbGroups, dictNbMembers, getGroupsFromProject(doc.id), getUsersFromProject('inside', doc.id), getUsersFromProject('notInside', doc.id))

    return project


def getGroupsFromProject(idProject):
    docs = db.collection(u'groups').stream()
    listGroups = []

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictIdProject = dictionnaryjson['idProject']
        dictMaxMember = dictionnaryjson['maxMember']
        dictPosition = dictionnaryjson['position']
        dictTitle = dictionnaryjson['title']
        if(idProject == dictIdProject):
            group = GroupModel(doc.id, dictIdProject, dictMaxMember, dictTitle, dictPosition, getUsersInsideGroup(doc.id))
            listGroups.append(group)


    return listGroups

def getGroup(idGroup):
    docs = db.collection(u'groups').stream()
    group = ""

    for doc in docs:
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictIdProject = dictionnaryjson['idProject']
        dictMaxMember = dictionnaryjson['maxMember']
        dictPosition = dictionnaryjson['position']
        dictTitle = dictionnaryjson['title']
        if(idGroup == doc.id):
            group = GroupModel(doc.id, dictIdProject, dictMaxMember, dictTitle, dictPosition, getUsersInsideGroup(doc.id))


    return group

def getUsers():
    docs = db.collection(u'persons').stream()
    listUsers = []

    for doc in docs:
        
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']

        user = UserModel(doc.id, dictName, dictAdmin)
        listUsers.append(user)

    return listUsers

def getUser(userId):
    docs = db.collection(u'persons').stream()

    for doc in docs:
        
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']
        if(userId == doc.id):
            user = UserModel(doc.id, dictName, dictAdmin)

    return user

def getUsersFromProject(typeOfResearch, idProject):
    arrayOfidUserInProject = []
    docs = db.collection(u'list_users_projects').stream()

    for doc in docs:
        
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictIdProject = dictionnaryjson['idProject']
        dictIdUser = dictionnaryjson['idUser']

        if(dictIdProject == idProject):
            arrayOfidUserInProject.append(dictIdUser)


    docs = db.collection(u'persons').stream()
    listUsersInside = []
    listUsersNotInside = []

    for doc in docs:
        
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("True", "true")
        dictionnary = dictionnary.replace("False", "false")
        dictionnary = dictionnary.replace("'", '"')
        dictionnaryjson = json.loads(dictionnary)
        dictName = dictionnaryjson['name']
        dictAdmin = dictionnaryjson['isAdmin']

        user = UserModel(doc.id, dictName, dictAdmin)
        if(dictAdmin == False):
            if(doc.id in arrayOfidUserInProject):
                listUsersInside.append(user)
            else:
                listUsersNotInside.append(user)

    if(typeOfResearch == "inside"):
        return listUsersInside
    else:
        return listUsersNotInside

def getProjectInsideOrOutside(typeOfProject):
    listProject = getProjects()
    listProjectNotInside = []
    listProjectInside = []

    for project in listProject:
        thisProjectUsersInside = getUsersFromProject('inside', project.id)
        thisProjectListId = []
        for user in thisProjectUsersInside:
            thisProjectListId.append(user.id)
        if(settings.idUser in thisProjectListId):
            listProjectInside.append(project)
        else:
            listProjectNotInside.append(project)

    
    if(typeOfProject == 'inside'):
        return listProjectInside
    else:
        return listProjectNotInside

def getUsersInsideGroup(groupId):
    listUsers = []
    docs = db.collection(u'list_users_groups').stream()

    for doc in docs:
        
        dictionnary = str(doc.to_dict())
        dictionnary = dictionnary.replace("{'", '{"')
        dictionnary = dictionnary.replace("'}", '"}')
        dictionnary = dictionnary.replace("': '", '": "')
        dictionnary = dictionnary.replace("', '", '", "')

        dictionnary = dictionnary.replace("':", '":')
        dictionnary = dictionnary.replace("',", '",')

        dictionnary = dictionnary.replace(", '", ', "')
        dictionnary = dictionnary.replace(": '", ': "')

        dictionnaryjson = json.loads(dictionnary)
        dictIdGroup = dictionnaryjson['idGroup']
        dictIdUser = dictionnaryjson['idUser']

        if(dictIdGroup == groupId):
            listUsers.append(getUser(dictIdUser))

    return listUsers

def addUserToGroup(userId, groupId):
    group = getGroup(groupId)
    project = getProject(group.idProject)

    # Verification
    messageToReturn = ''
    userInProject = False
    userAlreadyInProjectGroup = False


    # Verification if group is already in this maximum
    if(group.maxMember <= group.intListUserInside):
        messageToReturn = 'Le groupe est au complet'
    else: 
        # Search if current user are already in the project
        for user in project.listUserInside:
            if (user.id == userId):
                userInProject = True

        # Search if current user are already in one group related to the project
        for groupInList in project.listGroups:
            for userInList in groupInList.listUserInside:
                if (userInList.id == userId):
                    userAlreadyInProjectGroup = True

        
        
        if (userAlreadyInProjectGroup == True):
            messageToReturn = "L'utilisateur semble déjà participer à un autre groupe pour le même projet"
        else:
            if(userInProject == True):
                db.collection('list_users_groups').add({'idGroup': groupId, 'idUser': userId, 'idProject': group.idProject})
                messageToReturn = "L'utilisateur a été ajouté au groupe"
            else:
                if(project.intListUserInside >= project.nbGroups):
                    messageToReturn = "L'utilisateur ne peut pas rejoindre ce groupe, car le projet où se situe ce groupe est déjà complet !"
                else:
                    db.collection('list_users_projects').add({'idProject': group.idProject, 'idUser': userId})
                    db.collection('list_users_groups').add({'idGroup': groupId, 'idUser': userId, 'idProject': group.idProject})
                    messageToReturn = "L'utilisateur a été ajouté au projet et au groupe"

    return messageToReturn

def getUsersInProjectNoAffected(groupId):
    group = getGroup(groupId)
    project = getProject(group.idProject)

    listUsersIdHaveGroup = []
    listUsersInProject = []
    listUsersWithoutGroup = []

    # Search all users with already one group
    for groupInList in project.listGroups:
        for userInList in groupInList.listUserInside:
            listUsersIdHaveGroup.append(userInList.id)

    # Search all users in the project
    for userInList in project.listUserInside:
        listUsersInProject.append(userInList)


    # Create list of users without users already in group
    for userInList in listUsersInProject:
        if(listUsersIdHaveGroup.count(userInList.id) == 0):
            listUsersWithoutGroup.append(userInList)


    return listUsersWithoutGroup



    






    










    


































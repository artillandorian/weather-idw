from fastapi import FastAPI, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles
from models.activity_model import ActivityModel


import uvicorn
import requests as re
import json

def createListActivity(data):
    listActivity = []

    for i in data:
        _key = i['_key']
        title = i['title']
        description = i['description']
        isPro = i['isPro']
        coordinates = i['coordinates']
        address = i['address']
        price = i['price']
        beginningDate = i['beginningDate']
        duration = i['duration']
        bookable = i['bookable']
        nbPlaces = i['nbPlaces']
        handler = i['handler']
        participants = i['participants']
        imagesPaths = i['imagesPaths']
        tags = i['tags']

        newActivity = ActivityModel(_key, title, description, isPro, coordinates, address, price, beginningDate, duration, bookable, nbPlaces, handler, participants, imagesPaths, tags)
        
        listActivity.append(newActivity)

    return listActivity

def selectLanguage(selector):

    #   French selection
    if (selector == "en"):
        l = open('language/en.json',) 
    #   English selection
    else:
        l = open('language/fr.json',) 
        
    language = json.load(l)
    return language

def userLogin(phone, password):
    headers = {'Accept': 'application/json', 'realm': 'user'}

    data = '{"phone_number":"' + phone + '", "password":"' + password + '"}'

    response = re.post('https://gateway.ad1838.ovh:10443/login', headers=headers, data=data)

    print(response.json())
    print(response.status_code)

    return {"response": response.json(), "status":response.status_code}